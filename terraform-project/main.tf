# Resource group
resource "azurerm_resource_group" "terraform_gifts_rg" {
  name     = "${var.resource_group_name}${var.project_name}"
  location = "West Europe"

}

#virtual network
resource "azurerm_virtual_network" "terraform_gifts_vnet" {
  name                = "terraform-gifts-WestEurope-vnet"
  resource_group_name = azurerm_resource_group.terraform_gifts_rg.name
  location            = azurerm_resource_group.terraform_gifts_rg.location
  address_space       = ["10.0.0.0/16"]
}

# subnet for web host 
resource "azurerm_subnet" "web_subnet" {
  virtual_network_name = azurerm_virtual_network.terraform_gifts_vnet.name
  name                 = "web-subnet"
  resource_group_name  = azurerm_resource_group.terraform_gifts_rg.name
  address_prefixes     = ["10.0.1.0/24"]
}


# subnet for db host 
resource "azurerm_subnet" "db_subnet" {
  virtual_network_name = azurerm_virtual_network.terraform_gifts_vnet.name
  name                 = "db-subnet"
  resource_group_name  = azurerm_resource_group.terraform_gifts_rg.name
  address_prefixes     = ["10.0.2.0/24"]
}

# network security group for web
resource "azurerm_network_security_group" "nsg_gifts_web_dev" {
  name                = "nsg-gifts-web-dev"
  location            = azurerm_resource_group.terraform_gifts_rg.location
  resource_group_name = azurerm_resource_group.terraform_gifts_rg.name

  security_rule {
    name                       = "AllowPortsCustomInbound"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "8080"
    source_address_prefix      = "*"
    destination_address_prefix = "*"

  }

  security_rule {
    name                       = "Allow_Web_Port_80"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }


  security_rule {
    name                       = "Allow_Web_Port_5000"
    priority                   = 102
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "5000"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }




  security_rule {
    name                       = "AllowCustominbound"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*" # only my public ip adress
    destination_address_prefix = "*"
  }
}

#associate the network security group with the web subnet
resource "azurerm_subnet_network_security_group_association" "web_nsg_association" {
  subnet_id                 = azurerm_subnet.web_subnet.id
  network_security_group_id = azurerm_network_security_group.nsg_gifts_web_dev.id
}

#network security group for db
resource "azurerm_network_security_group" "nsg_gifts_db_dev" {
  name                = "nsg-gifts-db-dev"
  location            = azurerm_resource_group.terraform_gifts_rg.location
  resource_group_name = azurerm_resource_group.terraform_gifts_rg.name

  security_rule {
    name                       = "AllowCidrBlockCustomInbound"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "5432"
    source_address_prefix      = "10.0.1.0/24"
    destination_address_prefix = "*"
  }


  security_rule {
    name                       = "AllowCustominbound"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*" # only my public ip adress
    destination_address_prefix = "*"
  }
}

#associate the network security group with the db subnet
resource "azurerm_subnet_network_security_group_association" "db_nsg_association" {
  subnet_id                 = azurerm_subnet.db_subnet.id
  network_security_group_id = azurerm_network_security_group.nsg_gifts_db_dev.id
}

#public ip for web host 
resource "azurerm_public_ip" "web_public_ip" {
  name                = "web-public-ip"
  resource_group_name = azurerm_resource_group.terraform_gifts_rg.name
  location            = azurerm_resource_group.terraform_gifts_rg.location
  allocation_method   = "Static"
}

#network interface for web host
resource "azurerm_network_interface" "web_nic" {
  name                = "web_nic"
  resource_group_name = azurerm_resource_group.terraform_gifts_rg.name
  location            = azurerm_resource_group.terraform_gifts_rg.location
  ip_configuration {
    name                          = "web-ip-config"
    subnet_id                     = azurerm_subnet.web_subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.web_public_ip.id

  }
}

#public ip for postgreSQL db
resource "azurerm_public_ip" "db_public_ip" {
  name                = "db_public_ip"
  resource_group_name = azurerm_resource_group.terraform_gifts_rg.name
  location            = azurerm_resource_group.terraform_gifts_rg.location
  allocation_method   = "Static"
}

# network interface for postgreSQL db
resource "azurerm_network_interface" "db_nic" {
  name                = "db-nic"
  resource_group_name = azurerm_resource_group.terraform_gifts_rg.name
  location            = azurerm_resource_group.terraform_gifts_rg.location
  ip_configuration {
    name                          = "ipconfig-db-private"
    subnet_id                     = azurerm_subnet.db_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = "10.0.2.10"
    primary                       = true
  }


  ip_configuration {
    name                          = "db-ip-config"
    subnet_id                     = azurerm_subnet.db_subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.db_public_ip.id

  }


}


resource "azurerm_virtual_machine" "web_vm" {
  name                          = "web-vm"
  location                      = azurerm_resource_group.terraform_gifts_rg.location
  resource_group_name           = azurerm_resource_group.terraform_gifts_rg.name
  network_interface_ids         = [azurerm_network_interface.web_nic.id]
  vm_size                       = "Standard_DS1_v2"
  delete_os_disk_on_termination = var.delete_os_disk_on_termination

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "web-os-disk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = "web-vm"
    admin_username = var.vm_username
  }
  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/randy/.ssh/authorized_keys"
      key_data = var.web_public_key
    }
  }
  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      host        = azurerm_public_ip.web_public_ip.ip_address
      user        = var.vm_username
      private_key = file("/home/linik/Desktop/ssh-keys/web-ssh-key")
    }
    inline = [
      "sleep 10",
      "sudo apt-get update",
      "sudo apt-get install -y python3-pip",
      "sudo apt install -y libpq-dev",
      "sudo -H pip3 install flask",
      "sudo -H pip3 install psycopg2-binary",
      "sudo -H pip3 install pip install flask-cors",
      "echo 'export DB_PASSWORD=\"${var.db_vm_password}\"' >> ~/.bashrc",
      "echo '${data.template_file.app_py.rendered}' >> app.py",
      "python3 app.py",

    ]
  }
}

resource "azurerm_virtual_machine" "db_vm" {
  name                             = "db-vm"
  location                         = azurerm_resource_group.terraform_gifts_rg.location
  resource_group_name              = azurerm_resource_group.terraform_gifts_rg.name
  network_interface_ids            = [azurerm_network_interface.db_nic.id]
  vm_size                          = "Standard_DS1_v2"
  delete_os_disk_on_termination    = var.delete_os_disk_on_termination
  delete_data_disks_on_termination = true



  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "db-os-disk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  storage_data_disk {
    name              = "db_data_disk"
    managed_disk_type = "Standard_LRS"
    create_option     = "Empty"
    disk_size_gb      = 4
    lun               = "1"

  }
  os_profile {
    computer_name  = "db-vm"
    admin_username = var.vm_username
  }
  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/randy/.ssh/authorized_keys"
      key_data = var.db_public_key
    }
  }


  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      host        = azurerm_public_ip.db_public_ip.ip_address
      user        = var.vm_username
      private_key = file("/home/linik/Desktop/ssh-keys/db-ssh-key")
    }
    inline = [
      "sleep 10",
      "sudo apt-get update",
      "sudo apt-get install -y postgresql",
      # "sudo mkfs -t ext4 /dev/sdc",
      # "sudo mkdir /mnt/data",
      # "sudo mount /dev/sdc /mnt/data",
      # "sudo chown -R postgres:postgres /mnt/data",
      # "sudo sed -i 's|/var/lib/postgresql/10/main|/mnt/data|g' /etc/postgresql/10/main/postgresql.conf",
      "sudo service postgresql start",
      "sleep 10",
      "sudo sed -i '/^# IPv4 local connections:/a host    all             all             10.0.2.0/24             trust' /etc/postgresql/10/main/pg_hba.conf",
      "sudo sed -i \"s/^#listen_addresses = .*$/listen_addresses = '*'/\" /etc/postgresql/10/main/postgresql.conf",
      "sudo service postgresql restart",
      "sudo -i -u postgres psql -c ALTER USER postgres PASSWORD '${var.db_vm_password}';",
      "sudo -u postgres psql -c \"CREATE DATABASE gymdb;\"",
      "sudo -u postgres psql -d gymdb -c \"CREATE TABLE data (name VARCHAR, weight_value INTEGER, mytime TIMESTAMP);\"",
      "sudo -u postgres psql -c \"\\q\"",
      "sudo service postgresql restart",
      "psql -U postgres -d gymdb;"
    ]

  }
}
data "template_file" "app_py" {
  template = file("${path.module}/app.py")

}


output "web_subnet_name" {
  value       = azurerm_subnet.web_subnet.name
  description = "web subnet name"
}

output "db_subnet_name" {
  value       = azurerm_subnet.db_subnet.name
  description = "db subnet name"
}

output "web_vm_ip_address" {
  value       = azurerm_public_ip.web_public_ip.ip_address
  description = "Public IP address of the web VM"
}


output "db_vm_ip_address" {
  value       = azurerm_public_ip.db_public_ip.ip_address
  description = "Public IPaddress of the DB VM"
}

output "db_vm_data_disk" {
  value       = azurerm_virtual_machine.db_vm.storage_data_disk.0.name
  description = "Data disk name of the DB VM"
}

output "web_vm_data_disk" {
  value       = null
  description = "Web VM does not hame a data disk"
}
