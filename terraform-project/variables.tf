variable "project_name" {
  type        = string
  description = "Project Name"
  default     = "terraform-gifts"
}

variable "resource_group_name" {
  type        = string
  description = "Resource Group Name"
  default     = "rg-"
}


variable "vm_username" {
  type        = string
  description = "username"
  default     = "randy"
}


variable "delete_os_disk_on_termination" {
  description = "Delete datadisk when machine is terminated"
  default     = true
}

variable "db_vm_password" {
  type        = string
  description = "db vm password"
}

variable "web_vm_password" {
  type        = string
  description = "web vm password"
}


variable "db_public_key" {
  type        = string
  description = "db public key"
}

variable "web_public_key" {
  type        = string
  description = "web public key"
}


